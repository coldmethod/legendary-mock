package com.cold.legendary.mock.test.demo.service;

import org.springframework.stereotype.Service;

/**
 * @author cold
 * @date 2022/3/15
 */
@Service
public class RealSonService {

    public String callOK() {
        return "OkReal!!!";
    }
}
