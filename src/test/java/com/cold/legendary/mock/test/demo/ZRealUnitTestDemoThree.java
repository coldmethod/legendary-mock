package com.cold.legendary.mock.test.demo;

import com.alibaba.fastjson.JSON;
import com.cold.legendary.mock.LMock;
import com.cold.legendary.mock.annotation.LMockInjectBean;
import com.cold.legendary.mock.test.demo.dto.UserDto;
import com.cold.legendary.mock.test.demo.service.FatherTestService;
import com.cold.legendary.mock.test.demo.service.RealFatherTestService;
import com.cold.legendary.mock.test.demo.service.SonService;
import org.junit.After;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * @author cold
 * @date 2021/12/23
 */
public class ZRealUnitTestDemoThree extends TestSupportTransaction {

    @Resource
    private RealFatherTestService realFatherTestService;
    @Resource
    private FatherTestService fatherTestService;

    @Test
    public void testRealDemo() {

        String callReal = realFatherTestService.callReal();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(">>>>>>>callReal=" + callReal);
    }


    @Test
    public void testWCA() {
        UserDto wcaUser = fatherTestService.getUser();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(">>>>>>>wcaUser=" + JSON.toJSONString(wcaUser));
    }
}
