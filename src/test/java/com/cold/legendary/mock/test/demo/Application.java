package com.cold.legendary.mock.test.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author sylorl
 * @date 2017-10-26 18:09:44
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class},
        scanBasePackages = {
                "com.cold.legendary.mock.test.demo"},
        scanBasePackageClasses = {Application.class})
@EnableAspectJAutoProxy(exposeProxy = true)
public class Application implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    private static volatile boolean running = true;

    public static void main(String[] args) {
        try {
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    synchronized (Application.class) {
                        running = false;
                        Application.class.notify();
                    }
                }
            });
            System.setProperty("spring.devtools.restart.enabled", "false");
            System.setProperty("spring.boot.mainclass", Application.class.getName());
            SpringApplication.run(Application.class, args);
        } catch (RuntimeException e) {
            logger.error(e.getMessage(), e);
            System.exit(1);
        }

        synchronized (Application.class) {
            while (running) {
                try {
                    Application.class.wait();
                } catch (Throwable e) {
                }
            }
        }
    }

    public void run(String... args) throws Exception {
        //do anything after SpringApplication process successfully.
    }
}
