package com.cold.legendary.mock.test.demo.service;

import com.cold.legendary.mock.test.demo.dto.UserDto;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author cold
 * @date 2022/1/19
 */
@Service
public class FatherTestService {
    @Resource
    private SonService sonService;

    public UserDto getUser() {
        UserDto qryUserDto = sonService.getInfo();
        return qryUserDto;
    }

    public String askNice() {
        return sonService.askNice();
    }
}
