package com.cold.legendary.mock.test.demo;

import com.alibaba.fastjson.JSON;
import com.cold.legendary.mock.LMock;
import com.cold.legendary.mock.annotation.LMockInjectBean;
import com.cold.legendary.mock.test.demo.dto.UserDto;
import com.cold.legendary.mock.test.demo.service.FatherTestService;
import com.cold.legendary.mock.test.demo.service.SonService;
import org.junit.After;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * @author cold
 * @date 2021/12/23
 */
public class UnitTestDemo extends TestSupportTransaction {

    @Resource
    @LMockInjectBean(mockBeanClazzes = {SonService.class})
    private FatherTestService fatherTestService;

    private LMock lMock = new LMock();

    @Test
    public void testDemo() {

        //单元测试mock方法的第一次和第三次调用,第二次正常调用


        lMock = lMock.startPreMock(fatherTestService, SonService.class, "getInfo");
        lMock.resultMock(1, UserDto
                .builder()
                .idNo("mockIdNo-1")
                .name("mockName-1")
                .build());

        lMock.resultMock(3, UserDto
                .builder()
                .idNo("mockIdNo-3")
                .name("mockName-3")
                .build());

        UserDto userDtoOne = fatherTestService.getUser();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(">>>>>>>result=" + JSON.toJSONString(userDtoOne));
        UserDto userDtoTwo = fatherTestService.getUser();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(">>>>>>>result=" + JSON.toJSONString(userDtoTwo));
        UserDto userDtoThree = fatherTestService.getUser();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(">>>>>>>result=" + JSON.toJSONString(userDtoThree));
    }
}
