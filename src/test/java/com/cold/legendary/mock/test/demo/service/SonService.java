package com.cold.legendary.mock.test.demo.service;

import com.cold.legendary.mock.test.demo.dto.UserDto;
import org.springframework.stereotype.Service;

/**
 * @author cold
 * @date 2022/1/19
 */
@Service
public class SonService {

    public UserDto getInfo() {
        return UserDto
                .builder()
                .idNo("realIdNo")
                .name("realName")
                .build();
    }

    public String askNice() {
        return "real nice!";
    }
}
