package com.cold.legendary.mock.test.demo.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author cold
 * @date 2022/1/19
 */
@Data
@Builder
public class UserDto {

    private String idNo;
    private String name;
}
