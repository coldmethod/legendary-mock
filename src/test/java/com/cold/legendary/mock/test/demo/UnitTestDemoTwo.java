package com.cold.legendary.mock.test.demo;

import com.alibaba.fastjson.JSON;
import com.cold.legendary.mock.LMock;
import com.cold.legendary.mock.annotation.LMockInjectBean;
import com.cold.legendary.mock.test.demo.dto.UserDto;
import com.cold.legendary.mock.test.demo.service.FatherTestService;
import com.cold.legendary.mock.test.demo.service.RealFatherTestService;
import com.cold.legendary.mock.test.demo.service.SonService;
import org.junit.After;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * @author cold
 * @date 2021/12/23
 */
public class UnitTestDemoTwo extends TestSupportTransaction {

    @Resource
    @LMockInjectBean(mockBeanClazzes = {SonService.class})
    private FatherTestService fatherTestService;

    @Resource
    private RealFatherTestService realFatherTestService;

    private LMock lMock = new LMock();

    @Test
    public void testDemo() {

        //单元测试mock方法的第一次和第三次调用,第二次正常调用

        lMock = lMock.startPreMock(fatherTestService, SonService.class, "askNice");
        lMock.resultMock(1, "mockNice");

        String result = fatherTestService.askNice();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(">>>>>>>result=" + result);


//        String callReal = realFatherTestService.callReal();
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
//        System.out.println(">>>>>>>callReal=" + callReal);
    }
}
