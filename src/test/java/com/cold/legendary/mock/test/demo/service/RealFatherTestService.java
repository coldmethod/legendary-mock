package com.cold.legendary.mock.test.demo.service;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author cold
 * @date 2022/3/15
 */
@Service
public class RealFatherTestService {
    @Resource
    private RealSonService realSonService;

    public String callReal() {
        return realSonService.callOK();
    }
}
