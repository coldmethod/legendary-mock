package com.cold.legendary.mock.aware;

import com.cold.legendary.mock.annotation.LMockInjectBean;
import com.cold.legendary.mock.context.whole.WholeMockContext;

import java.util.Arrays;
import java.util.List;

/**
 * @author cold
 * @since  2021/12/24
 */
public class MockInjectAware {

    private volatile static MockInjectAware mockInjectAware;

    public static MockInjectAware getInstance() {
        if (null == mockInjectAware) {
            synchronized (MockInjectAware.class) {
                if (null == mockInjectAware) {
                    mockInjectAware = new MockInjectAware();
                }
            }
        }
        return mockInjectAware;
    }

    /**
     * 加载所有的mock inject beans
     * @param mockInjectBeanClazz mockInjectBeanClazz
     * @param mockInjectBeanObject mockInjectBeanObject
     * @param lMockInjectBean lMockInjectBean
     */
    public void loadInjectMockBeans(Class mockInjectBeanClazz, Object mockInjectBeanObject, LMockInjectBean lMockInjectBean) {
        //加载一次当前的mockInjectBeanObject
        Class[] mockBeanClazzes = lMockInjectBean.mockBeanClazzes();
        List<Class> mockBeanClazzLs = Arrays.asList(mockBeanClazzes);

        //注册到全局上下文中
        WholeMockContext wholeMockContext = WholeMockContext.getInstance();
        wholeMockContext.register(mockInjectBeanClazz, mockInjectBeanObject, mockBeanClazzLs);
    }
}
