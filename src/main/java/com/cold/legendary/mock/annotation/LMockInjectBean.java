package com.cold.legendary.mock.annotation;

import java.lang.annotation.*;

/**
 * @author cold
 * @since  2021/12/24
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented()
public @interface LMockInjectBean {

    /**
     * 需要mock的bean的名称注解
     * @return 类
     */
    Class[] mockBeanClazzes();
}
