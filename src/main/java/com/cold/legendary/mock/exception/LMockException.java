package com.cold.legendary.mock.exception;

/**
 * @author cold
 * @since  2022/1/24
 */
public class LMockException extends RuntimeException {

    private String code;
    private String msg;

    public LMockException(String msg) {
        this.msg = msg;
    }
}
