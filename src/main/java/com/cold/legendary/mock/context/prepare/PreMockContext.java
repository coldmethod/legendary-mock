package com.cold.legendary.mock.context.prepare;

import com.cold.legendary.mock.context.prepare.element.PreMockBeanElement;
import com.cold.legendary.mock.context.prepare.element.PreMockElement;
import com.cold.legendary.mock.context.prepare.element.SinglePreMockElement;
import com.cold.legendary.mock.context.runnable.element.RunMockBeanElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author cold
 * @since 2022/1/18
 */
@Builder
@Data
@AllArgsConstructor
public class PreMockContext {

    /**
     * mock元素列表
     */
    private List<PreMockElement> preMockElements;

    public void addPreMockElement(SinglePreMockElement singlePreMockElement) {
        if (null == singlePreMockElement) {
            return;
        }
        PreMockElement exsitElement = preMockElements.stream().filter(e -> e.getPreMockInjectBeanName()
                .equals(singlePreMockElement.getPreMockInjectBeanObj().getClass().getName())).findFirst().orElse(null);

        if (null != exsitElement) {
            List<PreMockBeanElement> preMockBeanElements = exsitElement.getPreMockBeanElements();
            if (!CollectionUtils.isEmpty(preMockBeanElements)) {
                PreMockBeanElement exsitPreElement = preMockBeanElements.stream().filter(e ->
                        e.getPreMockBeanName().equals(singlePreMockElement.getPreMockBeanClazz().getName())
                                && e.getPreMockBeanMethodName().equals(singlePreMockElement.getPreMockBeanMethodName())
                                && e.getPreRunTimes().equals(singlePreMockElement.getPreRunTimes()))
                        .findFirst().orElse(null);
                if (null != exsitPreElement) {
                    //预加载元素已经存在了,不再加载
                    return;
                }else {
                    PreMockBeanElement addPreMockBeanElement = PreMockBeanElement.builder()
                            .preMockBeanName(singlePreMockElement.getPreMockBeanClazz().getName())
                            .preMockBeanMethodName(singlePreMockElement.getPreMockBeanMethodName())
                            .preRunTimes(singlePreMockElement.getPreRunTimes())
                            .preMockResult(singlePreMockElement.getPreMockResult())
                            .build();

                    List<PreMockBeanElement> pkElements = new ArrayList<>();
                    pkElements.addAll(exsitElement.getPreMockBeanElements());
                    pkElements.add(addPreMockBeanElement);
                    exsitElement.setPreMockBeanElements(pkElements);
                }
            }
        } else {
            PreMockBeanElement addPreMockBeanElement = PreMockBeanElement.builder()
                    .preMockBeanName(singlePreMockElement.getPreMockBeanClazz().getName())
                    .preMockBeanMethodName(singlePreMockElement.getPreMockBeanMethodName())
                    .preRunTimes(singlePreMockElement.getPreRunTimes())
                    .preMockResult(singlePreMockElement.getPreMockResult())
                    .build();
            PreMockElement addPreMockElement = PreMockElement.builder()
                    .preMockInjectBeanName(singlePreMockElement.getPreMockInjectBeanObj().getClass().getName())
                    .preMockBeanElements(Arrays.asList(addPreMockBeanElement))
                    .build();
            preMockElements.add(addPreMockElement);
        }
    }

}
