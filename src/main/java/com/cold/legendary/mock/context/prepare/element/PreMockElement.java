package com.cold.legendary.mock.context.prepare.element;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 运行时mock元素
 *
 * @author cold
 * @since 2021/12/24
 */
@Data
@Builder
public class PreMockElement {
    /**
     * 被注入mock的bean的名字
     */
    private String preMockInjectBeanName;
    /**
     * 预加载bean元素
     */
    private List<PreMockBeanElement> preMockBeanElements;
}
