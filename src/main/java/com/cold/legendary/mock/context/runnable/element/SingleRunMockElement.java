package com.cold.legendary.mock.context.runnable.element;

import lombok.Builder;
import lombok.Data;

/**
 * 运行时mock元素
 *
 * @author cold
 * @since 2021/12/24
 */
@Data
@Builder
public class SingleRunMockElement {
    /**
     * 被注入mock的bean
     */
    private String runMockInjectBeanName;
    /**
     * 需要mock的bean的class
     */
    private String runMockBeanName;
    /**
     * 需要mock的bean的参数名称
     */
    private String runMockBeanMethodName;

}
