package com.cold.legendary.mock.context.prepare.element;

import lombok.Builder;
import lombok.Data;

/**
 * 运行时mock元素
 *
 * @author cold
 * @since  2021/12/24
 */
@Data
@Builder
public class SinglePreMockElement {
    /**
     * 被注入mock的bean
     */
    private Object preMockInjectBeanObj;
    /**
     * 需要mock的bean的class
     */
    private Class preMockBeanClazz;
    /**
     * 需要mock的bean的参数名称
     */
    private String preMockBeanMethodName;
    /**
     * 运行次数
     */
    private Integer preRunTimes;
    /**
     * mock结果
     */
    private Object preMockResult;

}
