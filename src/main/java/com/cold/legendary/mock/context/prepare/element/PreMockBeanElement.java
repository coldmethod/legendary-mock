package com.cold.legendary.mock.context.prepare.element;

import lombok.Builder;
import lombok.Data;

/**
 * 运行时mock元素
 *
 * @author cold
 * @since  2021/12/24
 */
@Data
@Builder
public class PreMockBeanElement {
    /**
     * 需要mock的bean的名称
     */
    private String preMockBeanName;
    /**
     * 需要mock的bean的方法名称
     */
    private String preMockBeanMethodName;
    /**
     * 运行次数
     */
    private Integer preRunTimes;
    /**
     * mock结果
     */
    private Object preMockResult;

}
