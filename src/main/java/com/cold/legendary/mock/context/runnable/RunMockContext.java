package com.cold.legendary.mock.context.runnable;

import com.cold.legendary.mock.context.runnable.element.RunMockBeanElement;
import com.cold.legendary.mock.context.runnable.element.RunMockElement;
import com.cold.legendary.mock.context.runnable.element.SingleRunMockElement;
import lombok.Builder;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author cold
 * @since  2022/1/18
 */
@Builder
@Data
public class RunMockContext {

    /**
     * mock元素列表
     */
    private List<RunMockElement> runMockElements;


    /**
     * 添加运行计数
     * @param curSingleRunMockElement curSingleRunMockElement
     */
    public void addRunMockCount(SingleRunMockElement curSingleRunMockElement) {
        if (null == curSingleRunMockElement) {
            return;
        }

        RunMockElement exsitElement = runMockElements.stream().filter(e -> e.getMockInjectBeanName()
                .equals(curSingleRunMockElement.getRunMockInjectBeanName())).findFirst().orElse(null);

        if (null != exsitElement) {
            List<RunMockBeanElement> exsitRunElements = exsitElement.getRunMockBeanElements();
            if (!CollectionUtils.isEmpty(exsitRunElements)) {
                RunMockBeanElement exsitRunElement = exsitRunElements.stream().filter(e ->
                        e.getMockBeanName().equals(curSingleRunMockElement.getRunMockBeanName())
                                && e.getMockBeanMethodName().equals(curSingleRunMockElement.getRunMockBeanMethodName()))
                        .findFirst().orElse(null);
                if (null != exsitRunElement) {
                    Integer runTimes = exsitRunElement.getRunTimes();
                    runTimes = (null == runTimes) ? Integer.valueOf(0) : runTimes;
                    runTimes++;
                    exsitRunElement.setRunTimes(runTimes);
                    return;
                } else {
                    RunMockBeanElement addRunMockBeanElement = RunMockBeanElement.builder()
                            .mockBeanName(curSingleRunMockElement.getRunMockBeanName())
                            .mockBeanMethodName(curSingleRunMockElement.getRunMockBeanMethodName())
                            .runTimes(1)
                            .build();

                    List<RunMockBeanElement> rkElements = new ArrayList<>();
                    rkElements.addAll(exsitElement.getRunMockBeanElements());
                    rkElements.add(addRunMockBeanElement);
                    exsitElement.setRunMockBeanElements(rkElements);
                }
            }
        } else {
            RunMockBeanElement addRunMockBeanElement = RunMockBeanElement.builder()
                    .mockBeanName(curSingleRunMockElement.getRunMockBeanName())
                    .mockBeanMethodName(curSingleRunMockElement.getRunMockBeanMethodName())
                    .runTimes(1)
                    .build();
            RunMockElement addRunMockElement = RunMockElement.builder()
                    .mockInjectBeanName(curSingleRunMockElement.getRunMockInjectBeanName())
                    .runMockBeanElements(Arrays.asList(addRunMockBeanElement))
                    .build();
            runMockElements.add(addRunMockElement);
        }
    }

}
