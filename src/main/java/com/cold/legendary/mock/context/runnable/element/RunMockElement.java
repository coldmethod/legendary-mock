package com.cold.legendary.mock.context.runnable.element;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 运行时mock元素
 *
 * @author cold
 * @since 2021/12/24
 */
@Data
@Builder
public class RunMockElement {
    /**
     * 被注入mock的bean
     */
    private String mockInjectBeanName;
    /**
     * 运行bean元素
     */
    private List<RunMockBeanElement> runMockBeanElements;

}
