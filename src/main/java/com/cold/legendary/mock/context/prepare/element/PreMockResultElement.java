package com.cold.legendary.mock.context.prepare.element;

import lombok.Data;

/**
 * @author cold
 * @since 2021/12/24
 */
@Data
public class PreMockResultElement {
    /**
     * 调用次数
     */
    private int callTimes;
    /**
     * mock结果
     */
    private Object mockPreResult;
}
