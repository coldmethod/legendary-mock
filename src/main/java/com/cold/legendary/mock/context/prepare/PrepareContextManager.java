package com.cold.legendary.mock.context.prepare;


import com.cold.legendary.mock.context.prepare.element.SinglePreMockElement;

import java.util.ArrayList;

/**
 * 预加载上下文管理器
 * <p>
 * 预加载只需要维护预加载的信息即可,知道bean 方法 名字即可
 *
 * @author cold
 * @since 2022/2/14
 */
public class PrepareContextManager {

    private static ThreadLocal<PreMockContext> preMockContextThreadLocal = new ThreadLocal<>();

    public static void addPreMockElement(SinglePreMockElement singlePreMockElement) {

        if(null == preMockContextThreadLocal) {
            preMockContextThreadLocal = new ThreadLocal<>();
        }

        if (null == singlePreMockElement) {
            return;
        }
        PreMockContext preMockContext = preMockContextThreadLocal.get();
        if (null == preMockContext) {
            preMockContext = PreMockContext.builder().preMockElements(new ArrayList<>()).build();
        }
        preMockContext.addPreMockElement(singlePreMockElement);
        preMockContextThreadLocal.set(preMockContext);
    }

    public static PreMockContext getPreMockContext() {
        return preMockContextThreadLocal.get();
    }

    public static void cleanPreMockContext() {
        if(null != preMockContextThreadLocal) {
            preMockContextThreadLocal.remove();
        }
    }
}
