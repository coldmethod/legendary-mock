package com.cold.legendary.mock.context.runnable.element;

import lombok.Builder;
import lombok.Data;

/**
 * 运行时mock元素
 *
 * @author cold
 * @since 2021/12/24
 */
@Data
@Builder
public class RunMockBeanElement {
    /**
     * 需要mock的bean的class
     */
    private String mockBeanName;
    /**
     * 需要mock的bean的参数名称
     */
    private String mockBeanMethodName;
    /**
     * 运行次数
     */
    private Integer runTimes;

}
