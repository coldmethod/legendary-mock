package com.cold.legendary.mock.run;

import com.cold.legendary.mock.context.prepare.PrepareContextManager;
import com.cold.legendary.mock.context.prepare.element.PreMockResultElement;
import com.cold.legendary.mock.context.prepare.element.SinglePreMockElement;
import com.cold.legendary.mock.exception.LMockException;

import java.util.List;

/**
 * @author cold
 * @since  2021/12/24
 */
public class MockPreRunner {

    private SinglePreMockElement singlePreMockElement;

    public void prepareMock(Object preMockInjectBean, Class preMockBeanClazz, String preMockBeanMethodName) {
        singlePreMockElement = singlePreMockElement.builder()
                .preMockInjectBeanObj(preMockInjectBean)
                .preMockBeanClazz(preMockBeanClazz)
                .preMockBeanMethodName(preMockBeanMethodName)
                .build();
    }

    public void preMockResult(List<PreMockResultElement> preMockResultElements) {
        if (null == singlePreMockElement
                || null == singlePreMockElement.getPreMockInjectBeanObj()
                || null == singlePreMockElement.getPreMockBeanClazz()
                || null == singlePreMockElement.getPreMockBeanMethodName()) {
            throw new LMockException("preRunMockElement内容不存在!请先执行startMock()方法!");
        }

        for (PreMockResultElement preMockResultElement : preMockResultElements) {
            singlePreMockElement.setPreRunTimes(preMockResultElement.getCallTimes());
            singlePreMockElement.setPreMockResult(preMockResultElement.getMockPreResult());
            PrepareContextManager.addPreMockElement(singlePreMockElement);
        }

    }

}
