package com.cold.legendary.mock;

import com.cold.legendary.mock.context.prepare.PrepareContextManager;
import com.cold.legendary.mock.context.prepare.element.PreMockResultElement;
import com.cold.legendary.mock.context.runnable.RunContextManager;
import com.cold.legendary.mock.context.whole.WholeMockContext;
import com.cold.legendary.mock.run.MockPreRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cold
 * @since 2021/12/24
 */
public class LMock {

    private MockPreRunner mockPreRunner = new MockPreRunner();

    /**
     * 开始mock
     *
     * @param preMockInjectBean     preMockInjectBean
     * @param preMockBeanClazz      preMockBeanClazz
     * @param preMockBeanMethodName preMockBeanMethodName
     * @return LMock
     */
    public LMock startPreMock(Object preMockInjectBean, Class preMockBeanClazz, String preMockBeanMethodName) {
        mockPreRunner.prepareMock(preMockInjectBean, preMockBeanClazz, preMockBeanMethodName);
        return this;
    }

    /**
     * 单次调用mock结果
     *
     * @param mockCallTimes mockCallTimes
     * @param mockResult    mockResult
     * @return Lmock
     */
    public LMock resultMock(Integer mockCallTimes, Object mockResult) {
        List<PreMockResultElement> preMockResultElements = new ArrayList<>();
        PreMockResultElement preMockResultElement = new PreMockResultElement();
        preMockResultElement.setCallTimes(mockCallTimes);
        preMockResultElement.setMockPreResult(mockResult);
        preMockResultElements.add(preMockResultElement);
        mockPreRunner.preMockResult(preMockResultElements);
        return this;
    }

}
