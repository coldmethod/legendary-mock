# legendary-mock

#### 介绍
legendary-mock 是一款极简风格的单元测试mock框架,基于cglib动态代理来实现,用于单元测试过程中的spring bean的方法mock,源码简单,极易理解,作者试图化繁为简,让单测mock过程中不再被powermock的复杂逻辑和复杂方法所支配出的稀奇古怪的问题所困扰而无法解决浪费大量时间和精力.
 用百分之十的复杂度解决百分之九十的需求,那就砍掉剩余百分之十的需求. -- 凉

#### 软件架构
软件架构说明

核心组成:
1.预加载:
    1.1 上下文: 
            1.1.1 总体上下文: 结合单测启动,加载总体上下文,预存所有标记被mock的数据
            1.1.2 预加载上下文:单测运行,加载预加载上下文,预存当前运行的单测所需要被mock的数据
            1.1.3 运行上下文:单测运行到被mock的方法时,保存运行的相关信息;

2.动


#### 使用说明

1.  spring工程引入jar包,参考test UnitTestDemo.java 进行单测mock

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
